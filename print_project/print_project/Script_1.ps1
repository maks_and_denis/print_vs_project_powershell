﻿#
# Script1.ps1
#


#region My code.
$isDebug = $true
#$isDebug = $false


$invocation = $MyInvocation.MyCommand.Path
Write-Host "Invocation :" $invocation

$currentDir = Split-Path $invocation
Write-Host "CurrentDir :" $currentDir

$pathOutput = $currentDir + "\output.txt"
"" | Out-File $pathOutput


$mark = "/*****************************************************************************/"


$printProjects = New-Object System.Collections.ArrayList
$projectDirectories = @{}
$excludeFolders = @{}
$excludeExtensions = @{}
$excludeFoldersExtentions = @{}

$FileSettingsPath = $currentDir + "\settings.xml"
Write-Host "FileSettingsPath :" $FileSettingsPath

$ScriptParseXmlPath = $currentDir + "\Script_parse_xml.psm1"
Write-Host "ScriptParseXmlPath :" $ScriptParseXmlPath

Import-Module $ScriptParseXmlPath


[xml]$xml = Get-Content $FileSettingsPath
#$xml


ParseXml $xml $printProjects $projectDirectories $excludeExtensions $excludeFoldersExtentions


Write-Host "projectDirectories[0]" $projectDirectories[0]


foreach ($printProject in $printProjects) {

	Write-Host "printProject = " $printProject

	$fileDirectory = $projectDirectories[$printProject]
	Write-Host "fileDirectory = " $fileDirectory

	$projectName = Split-Path $fileDirectory -Leaf
	Write-Host "projectName = $projectName"

	$files = Get-ChildItem -Recurse -File $fileDirectory

	$isExclude = $false


	foreach ($file in $files) {

		$isExclude = $false

		$sourcePath = $file.FullName
		$sourceFileName = $file.Name
		$sourceFileExtension = $file.Extension

		$textFileName =  $file.FullName.Substring($file.FullName.IndexOf($projectName))
		$textDirectoryName =  $file.DirectoryName.Substring($file.DirectoryName.IndexOf($projectName))
		

		foreach ($excludeFoldersProject in $excludeFolders[$printProject]) {

			#Write-Host "ExcludeFolders[$printProject] : " $excludeFolders[$printProject]

			foreach ($excludeFolder in $excludeFoldersProject) {
				if ($textDirectoryName -ccontains $excludeFolder) {

					if ($isDebug) {
						"Exclude folder by ExcludeFolders: " + $textDirectoryName
					}
				
					$isExclude = $true
				}									
			}
		}
	
		if ($isExclude) {
			continue
		}

		foreach ($excludeExtensionProject in $excludeExtensions[$printProject]) {

			#Write-Host "ExcludeExtensions[$printProject] : " $excludeExtensions[$printProject]

			foreach ($excludeExtension in $excludeExtensionProject) {

				#Write-Host "ExcludeExtension : " $excludeExtension
				#Write-Host "SourceFileExtension : " $sourceFileExtension

				if ($sourceFileExtension -match $excludeExtension) {
				
					if ($isDebug) {
						"Exclude file by ExcludeExtensions: " + $textFileName
					}
				
					$isExclude = $true
				}
			}
		}	

		if ($isExclude) {
			continue
		}
	
		$excludeFoldersExtentionsKeys = $excludeFoldersExtentions[$printProject].Keys

		#Write-Host "ExcludeFoldersExtentionsKeys : " $excludeFoldersExtentionsKeys
	
		foreach ($excludeFolderExtention in $excludeFoldersExtentionsKeys) {

			#Write-Host "ExcludeFoldersExtention : " $excludeFolderExtention

			if ($textDirectoryName -ccontains $excludeFolderExtention) {

				#Write-Host "ExcludeFoldersExtention : " $excludeFolderExtention

				foreach ($excludeExtension in $excludeFoldersExtentions[$printProject][$excludeFolderExtention]) {

					#Write-Host "ExcludeExtension : " $excludeExtension

					if ($sourceFileExtension -ccontains $excludeExtension) {
						if ($isDebug) {
							"Exclude file by ExcludeFoldersExtentions: " + $textFileName
						}
						
						$isExclude = $true
					}
				}
			}
		}


		if (!$isExclude) {
			if ($isDebug) {
				$textDirectoryName + " : " + $textFileName
			}		
	
			$text = Get-Content $sourcePath -Raw

			$mark | Out-File -Append $pathOutput
			$textFileName | Out-File -Append $pathOutput	
			$mark | Out-File -Append $pathOutput
			"" | Out-File -Append $pathOutput

			$text | Out-File -Append $pathOutput

			"`n`n" | Out-File -Append $pathOutput
		}	
	}
}