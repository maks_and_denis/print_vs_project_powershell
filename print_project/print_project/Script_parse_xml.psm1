#
# Script_parse_xml.psm1
#


#region ParseXml
function ParseXml
{
	param($xml,		
		$excludeProjects,
		$projectDirectories,
		$excludeExtensions,
		$excludeFoldersExtentions)
  
	foreach ($project in $xml.Projects.Project) 
	{
		Write-Host


		$projectId = $project.id
		Write-Host "Project id: " $projectId

		$excludeProjects.Add($projectId)


		Write-Host "ProjectDirectories: "
		$projectDirectories[$projectId] = New-Object System.Collections.ArrayList

		foreach ($property in $project.ProjectDirectory)
		{
			$property.Property.'#text'
			$projectDirectories[$projectId].Add($property.Property.'#text')
		}


		Write-Host "ExcludeFolders: "
		$excludeFolders[$projectId] = New-Object System.Collections.ArrayList

		foreach ($property in $project.ExcludeFolders)
		{
			$property.Property.'#text'
			$excludeFolders[$projectId].Add($property.Property.'#text')
		}


		Write-Host "ExcludeExtensions: "
		$excludeExtensions[$projectId] = New-Object System.Collections.ArrayList

		foreach ($property in $project.ExcludeExtensions)
		{
			$property.Property.'#text'
			$excludeExtensions[$projectId].Add($property.Property.'#text')
		}


		Write-Host "ExcludeFoldersExtentions: "	
		$excludeFoldersExtentions[$projectId] = @{}

		foreach ($properties in $project.ExcludeFoldersExtentions.Properties)
		{
			$excludeFolder = $properties.Name
			Write-Host "ExcludeFolder: " $excludeFolder
			$excludeFoldersExtentions[$projectId][$excludeFolder] = New-Object System.Collections.ArrayList

			#Write-Host $properties.InnerXml

			Write-Host "ExcludeExtentions: "

			foreach ($property in $properties.Property)
			{
				#Write-Host $property.InnerXml

				$property.'#text'
				$excludeFoldersExtentions[$projectId][$excludeFolder].Add($property.'#text')
			}
		}


		#foreach ($property in $project.ExcludeFoldersExtentions.Properties)
		#{
		#	Write-Host $property.InnerXml

		#	$property.Property.'#text'
		#}
	}

}
#endregion


Export-ModuleMember -Function "ParseXml"